package cz.fi.muni.pa165.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.OneToMany;


@Entity
public class Cage {

	@Id
	@GeneratedValue
        private long id = -1;

	@Column(nullable=true)
        private String description;
        
        @Column(nullable=false)
	private int capacity;
	
	@OneToMany(mappedBy="cage")
	private Set<Pet> pets = new HashSet<Pet>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Set<Pet> getPets() {
		return pets;
	}

	public void setPets(Set<Pet> pets) {
		this.pets = pets;
	}

	@Override
	public String toString() {
		return "Cage [id=" + id + ", description=" + description
				+ ", capacity=" + capacity  + "]";
	}

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cage other = (Cage) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
	
}
