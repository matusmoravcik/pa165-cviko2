package cz.fi.muni.pa165;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import cz.fi.muni.pa165.entity.Cage;
import cz.fi.muni.pa165.entity.Pet;
import cz.fi.muni.pa165.entity.Pet.PetColor;
import java.util.HashSet;
import java.util.Set;

public class AppMain {


	public static void main(String[] args) throws SQLException {
		//The following line is here just to start up a in-memory database 
		new AnnotationConfigApplicationContext(DaoContext.class);
		
		System.out.println(" ****** STARTING PET STORE APPLICATOIN ****** ");
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myUnit");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Calendar cal = Calendar.getInstance();                
                
                Cage cage1 = new Cage();
		cage1.setCapacity(2);
                cage1.setDescription("A new, shiny, titanium cage.");
                
                Cage cage2 = new Cage();
		cage2.setCapacity(1); 
		
		Pet pet1 = new Pet();
		pet1.setName("Sisi");
		cal.set(2013, 1, 22);
		pet1.setBirthDate(cal.getTime());
		pet1.setColor(PetColor.BLACK);
                pet1.setCage(cage2);
                
                Pet pet2 = new Pet();
		pet2.setName("Ausii");
		cal.set(2012, 3, 20);
		pet2.setBirthDate(cal.getTime());
		pet2.setColor(PetColor.RED);
                pet2.setCage(cage1);
                
                Pet pet3 = new Pet();
		pet3.setName("Aron");
		cal.set(2010, 8, 14);
		pet3.setBirthDate(cal.getTime());
		pet3.setColor(PetColor.RED);
                pet3.setCage(cage2);
                
                Set<Pet> s1 = new HashSet<Pet>();
                s1.add(pet1);
                cage1.setPets(s1);
                
                Set<Pet> s2 = new HashSet<Pet>();
                s2.add(pet2);
                s2.add(pet3);
                cage2.setPets(s2);
	
		em.persist(pet1);
                em.persist(pet2);
                em.persist(pet3);
                
                em.persist(cage1);
                em.persist(cage2);
		em.getTransaction().commit();
		em.close();
		
		printAllPets(emf);
                findAllCages(emf);
    emf.close();
	}
	
	public static void printAllPets(EntityManagerFactory emf){
		System.out.println(" ********************************");
		System.out.println("        PET LIST      ");
		EntityManager em = emf.createEntityManager();
		List<Pet> pets = em.createQuery("SELECT p from Pet p",Pet.class).getResultList();
		
		for (Pet p : pets) {
			System.out.println(p);
		}
		
		em.close();		
	}
        
        public static void findAllCages(EntityManagerFactory emf){
            System.out.println(" ********************************");
		System.out.println("        CAGE LIST      ");
		EntityManager em = emf.createEntityManager();
		List<Cage> cages = em.createQuery("SELECT c from Cage c",Cage.class).getResultList();
		
		for (Cage c : cages) {
			System.out.println(c);
                        for (Pet p : c.getPets()) {
			System.out.println(p);                        
		}
		}
		
		em.close();
        }

}
